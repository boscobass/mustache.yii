# Changelog
This file contains highlights of what changes on each version of the [Mustache.yii](https://packagist.org/packages/cedx/yii2-mustache) library.

#### Version 0.3.0
- Breaking change: ported the library API to [Yii](http://www.yiiframework.com) version 2.
- Fixed [Bitbucket issue #1](https://bitbucket.org/cedx/mustache.yii/issue/1)
- Upgraded [Mustache](https://github.com/bobthecow/mustache.php) dependency to version 2.8.0.

#### Version 0.2.0
- Breaking change: ported the library API to [namespaces](http://php.net/manual/en/language.namespaces.php).

#### Version 0.1.1
- Added `CMustacheI18nHelper` helper for internationalization.
- Breaking change: moved `CMustacheHtmlHelper::getTranslate()` method to `CMustacheI18nHelper` class.
- Fixed [GitHub issue #1](https://github.com/cedx/mustache.yii/issues/1)
- Lowered the required [PHP](http://php.net) version.
- Upgraded [Mustache](https://github.com/bobthecow/mustache.php) dependency to version 2.7.0.

#### Version 0.1.0
- Initial release.
